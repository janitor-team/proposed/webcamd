#
# webcamd - Webcam Daemon
#
# Makefile
#

DESTDIR=

INSTALL=install

BINDIR=$(DESTDIR)/usr/bin

all:

install:
	$(INSTALL) -d -m 755 $(BINDIR)
	$(INSTALL) -m 755 webcamd $(BINDIR)
	$(INSTALL) -m 755 webcamd-setup $(BINDIR)

userinstall:
	$(INSTALL) -d -m 700 $(HOME)/.webcamd
	$(INSTALL) -m 700 webcamd.conf $(HOME)/.webcamd
	$(INSTALL) -m 700 index_up.html $(HOME)/.webcamd
	$(INSTALL) -m 700 index_down.html $(HOME)/.webcamd

uninstall:
	rm -f $(BINDIR)/webcamd

clean:
	rm -f *~